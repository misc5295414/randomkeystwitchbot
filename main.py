import os
import random
import threading
import game
import pydirectinput as pdi
from twitchio.ext import commands

keys = {"w": False, "a": False, "s": False, "d": False, "space": False}
bot = commands.Bot(
    # set up the bot
    irc_token=os.environ['TMI_TOKEN'],
    client_id=os.environ['CLIENT_ID'],
    nick=os.environ['BOT_NICK'],
    prefix=os.environ['BOT_PREFIX'],
    initial_channels=[os.environ['CHANNEL']],
)


@bot.event
async def event_ready():
    print(f"{os.environ['BOT_NICK']} is online!")


@bot.event
async def event_message(ctx):
    if ctx.author.name.lower() == os.environ["BOT_NICK"].lower():
        return
    await bot.handle_commands(ctx)


@bot.command(name="crazy")
async def crazy(ctx):
    pdi.PAUSE = 0.5
    for i in range(10):
        keyToPress = list(keys.keys())[random.randint(0, len(keys) - 1)]
        if not keys[keyToPress]:
            pdi.keyDown(keyToPress)
            keys[keyToPress] = True
        else:
            pdi.keyUp(keyToPress)
            keys[keyToPress] = False
    for k in keys.keys():
        keys[k] = False
        pdi.keyUp(k)


@bot.command(name="rect")
async def rect(ctx):
    game.rect()

bot_thread = threading.Thread(target=bot.run)
bot_thread.start()

game_thread = threading.Thread(target=game.start())
game_thread.start()


